$(document).ready(function() {
	menuToggle();

	// header scroll
	var header = $('#navHeader');
	var logo = $('.navbar-brand');
	var buttons = $('.header-buttons');
	$(window).scroll(function() {
		var scroll = $(window).scrollTop();
		if (scroll >= 100 && $(this).width() > 100) {
			header.addClass('fixed-top header-sticky');
			logo.addClass('small');
			logo.addClass('show-block');
			//buttons.removeClass('disabled');
		} else {
			header.removeClass('fixed-top header-sticky');
			logo.removeClass('show-block');
			logo.removeClass('small');
			//buttons.addClass('disabled');
		}
	});

	// slider
	$('.carousel').carousel({
		ride: true,
		interval: 5000,
		pause: false
	});

	// lightbox
	$(document).on('click', '[data-toggle="lightbox"]', function(event) {
		event.preventDefault();
		$(this).ekkoLightbox();
	});

	//load more
	$('.item').slice(0, 12).show();
	$('#loadMore').on('click', function(e) {
		e.preventDefault();
		$('.item:hidden').slice(0, 12).toggle(function() {
			$sidebar.animate({ marginTop: '0px' }, 400);
		});
		if ($('.item:hidden').length == 0) {
			$('#loadMore').text('Aguarde a próxima Corrida para mais fotos :)').addClass('disabled');
		}
	});

	//form
	$('#contact-form').validator();
	$('#contact-form').on('submit', function(e) {
		// if the validator does not prevent form submit
		if (!e.isDefaultPrevented()) {
			var url = 'arquivo do contato';

			var messageAlert = 'alert-success';
			var messageText = 'Mensagem enviada com sucesso';

			var alertBox =
				'<div class="alert ' +
				messageAlert +
				' alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' +
				messageText +
				'</div>';

			if (messageAlert && messageText) {
				// inject the alert to .messages div in our form
				$('#contact-form').find('.messages').html(alertBox);
				// empty the form
				$('#contact-form')[0].reset();
			}

			return false;
		}
	});
});

function menuToggle() {
	$('.menu-plate').on('click', function(e) {
		e.stopPropagation();
	});
	$('.menu-close, .menu-svg, .menu-overlay').on('click', function() {
		$('html').toggleClass('menu-open');
	});
}
